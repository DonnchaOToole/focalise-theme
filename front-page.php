<script>
  fbq('track', 'ViewContent');
</script>
<div class="hero-banner">
  <div class="container">
    <div class="cog-animation">
      <i class="fa fa-cog fa-spin fa-3x hero-cog-1"></i>
      <i class="fa fa-cog fa-2x hero-cog-2"></i>
    </div>
    <h1 class="hero-text">Get a website that drives business</h1>
    <p class="lead hero-subtext"><span class="bold">Bespoke</span>, mobile-ready websites designed to grow your business. </p>
    <a href="https://focalise.ie/contact/" class="btn btn-primary">Book your free consultation</a>
  </div>
</div>
<section class="web-design">
  <div class="container">
    <div class="row">
      <div class="col-md-10">
        <h2 class="spaced">Your One-Stop Shop for Web Design</h2>
        <p>Whether you're looking to revamp your portfolio, start a web store or turbo-charge your blog, we can help.</p>
        <a href="<?php echo get_home_url();?>/web-design/" class="btn btn-primary btn-lg mt-2">Web Design Services</a>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="branding pt-5">
    <h3>Branding</h3>
    <p class="lead">
      Your brand is your business and we love building brands.
      <br>Work with us to grow your brand and make an impact.
    </p>
  </div>

  <div class="landing-page-row row mt-5 mb-5">
    <div class="col-sm-8">
      <h3 class="spaced">Landing Page Design</h3>
      <p class="lead">Optimise your website to work in harmony with your ads.</p>
      <p>We build landing pages for each of the keywords you target with your AdWords Campaign.</p>
      <p>Well designed landing pages can boost your conversion rates and your AdRank score,
        <span class="bold">saving you money.</span>
      </p>
      <a href="https://focalise.ie/landing-page-design/" class="mt-2 btn btn-primary">More about landing pages</a>
    </div>
  </div>

  <section class="service-cards">
    <div class="row">
      <div class="col-lg-4 col-sm-6">
        <div class="card">
          <img alt="Image for web hosting" rel="preload" class="card-img-top img-fluid" src="<?php bloginfo('template_directory');?>/dist/images/web-hosting-square.jpg"
            alt="Web Hosting">
          <div class="card-block">
            <h4 class="card-title title">Web Hosting</h4>
            <p class="card-text">Your website needs somewhere to live. We'll keep it online and responding
              <span class="bold">quickly</span> to your customers.</p>
            <a class="btn btn-primary" href="<?php echo get_home_url();?>/web-hosting/">Read More</a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="card">
          <img alt="Image for search engine optimisation" rel="preload" class="card-img-top img-fluid" src="<?php bloginfo('template_directory');?>/dist/images/seo-4.jpg"
            alt="Search Engine Optimisation">
          <div class="card-block">
            <h4 class="card-title title">Search Engine Optimisation</h4>
            <p class="card-text">Improve your website and rank higher in the search results.</p>
            <a class="btn btn-primary pull-left mr-1 mt-1" href="<?php echo get_home_url();?>/seo-services/">SEO Services</a>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<section class="g-suite">
  <div class="container">
    <h3><i class="fa fa-envelope-o hvr-grow training-icon" aria-hidden="true"></i>G Suite</h3>
    <p class="lead">A complete suite of business tools keeping your business in sync and moving fast. Built by Google.</p>
    <a href="<?php echo get_home_url();?>/google-suite/" class="btn btn-primary btn-lg mt-2">
      <i class="fa fa-info-circle" aria-hidden="true"></i> More info</a>
  </div>
</section>

<section class="trading-online-voucher">
  <div class="container">
    <h3><i class="fa fa-money hvr-grow training-icon" aria-hidden="true"></i>Trading Online Voucher Scheme</h3>
    <p class="lead">Your business may be eligible for a government grant worth up to €2,500 to help you build your business online. </p>
    <a href="<?php echo get_home_url();?>/trading-online-voucher-scheme/" class="btn btn-primary btn-lg mt-2">
      <i class="fa fa-info-circle" aria-hidden="true"></i> More info</a>
  </div>
</section>

<section class="training">
  <div class="container">
    <h3>
      <i class="fa fa-rocket hvr-grow training-icon" aria-hidden="true"></i>Learn Digital Marketing</h3>
    <p class="lead">Practical courses in digital marketing to help you grow your business. </p>
    <a href="<?php echo get_home_url();?>/training/" class="btn btn-primary btn-lg mt-2">
      <i class="fa fa-info-circle" aria-hidden="true"></i> Courses</a>
  </div>
</section>

<section class="how-to-get-into-local-search-results">
  <div class="container">
    <h3>
      <i class="fa fa-map-marker hvr-grow how-to-get-into-local-search-results-icon" aria-hidden="true"></i>How to get into the local search results</h3>
    <p class="lead">When people search for services in their area, the local results box comes up. Wondering how to get your business listed in there?</p>
    <a href="<?php echo get_home_url();?>/local-search-optimisation-guide/" class="btn btn-primary btn-lg mt-2">
      <i class="fa fa-info-circle" aria-hidden="true"></i> Find out how</a>
  </div>
</section>


<?php get_template_part('templates/testimonials'); ?>

<section class="get-closer">
  <div class="container">
    <div class="text-center">
      <a class="mt-5 btn-lg btn btn-primary" href="<?php echo get_home_url();?>/contact/">Start your project</a>
    </div>
  </div>
</section>
</div>
