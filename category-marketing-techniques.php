<?php
/**
* Links Category Template
*/

?>
    <section id="primary" class="site-content container">
        <div id="content" role="main">
            <?php 
// Check if there are any posts to display
if ( have_posts() ) : ?>
                <header class="archive-header">
                    <div class="jumbotron">
                        <h1 class="archive-title">Marketing Techniques</h1>
                        <p>Make sure to <a href="https://www.youtube.com/channel/UCAo1-eYYQL7UzYgigO8hHVQ">subscribe to our YouTube channel</a> to get notifications for our new tutorials. We also do <a href="<?php echo get_home_url();?>/training">1 on 1 training.</a></p>
                    </div>
                    <?php
// Display optional category description
 if ( category_description() ) : ?>
                        <div class="archive-meta">
                            <?php echo category_description(); ?>
                        </div>
                        <?php endif; ?>
                </header>
                <?php

// The Loop
while ( have_posts() ) : the_post(); ?>
                    <div class="col-sm-12">
                        <div class="card">
                            <?php echo get_the_post_thumbnail( $page->ID, 'full', array('class' => 'card-img-top img-fluid') ); ?>
                                <div class="card-block">
                                    <a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute();?>" class="card-title">
                                        <h2><?php the_title(); ?></h2>
                                    </a>
                                    <div class="card-text">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!--                     <div class="entry">
                        <?php the_content(); ?>
                    </div> -->
                    <?php endwhile; 

else: ?>
                        <p>Sorry, no posts matched your criteria.</p>
                        <?php endif; ?>
        </div>
    </section>
