<div class="jumbotron jumbotron-fluid gradient-overlay mt-3">
  <div class="container">
    <h1 class="mt-4">About Focalise</h1>
    <p class="lead">We design and build digital experiences and services.
      <br> We're here to help you
      <span class="bold">build your brand</span> and
      <span class="bold">reach more customers</span>.</p>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-8">
      <p>Whether you're a small business, startup or SME - if you need help to grow your business with branding,
        <a href="https://focalise.ie/web-design/">web design</a> or digital marketing, then you've come to the right place.</p>
      <p>We've helped to grow businesses by increasing their brand awareness, developing their digital presence and growing
        their sales.</p>
      <p>We also provide
        <a href="https://focalise.ie/training/">digital marketing training</a>.</p>
      <a href="https://focalise.ie/contact/" class="btn btn-lg btn-primary">Get in touch</a>
    </div>
  </div>
  <section class="opening-hours">
    <h3>Opening Hours</h3>
    <table class="table table-striped table-hover table-sm">
      <thead>
        <tr>
          <th>Day</th>
          <th>Hours</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Monday</th>
          <td>9:00am - 5:30pm</td>
        </tr>
        <tr>
          <th scope="row">Tuesday</th>
          <td>9:00am - 5:30pm</td>
        </tr>
        <tr>
          <th scope="row">Wednesday</th>
          <td>9:00am - 5:30pm</td>
        </tr>
        <tr>
          <th scope="row">Thursday</th>
          <td>9:00am - 5:30pm</td>
        </tr>
        <tr>
          <th scope="row">Friday</th>
          <td>9:00am - 5:30pm</td>
        </tr>
        <tr>
          <th scope="row">Saturday</th>
          <td>Closed</td>
        </tr>
        <tr>
          <th scope="row">Sunday</th>
          <td>Closed</td>
        </tr>
      </tbody>
    </table>
  </section>
</div>

<?php get_template_part('templates/testimonials'); ?>
