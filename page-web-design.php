<img class="img-fluid mt-5" alt="focalise web design" src="<?php bloginfo('template_directory');?>/dist/images/focalise-web-design.jpg">
<section class="web-design-intro">
  <div class="container">
    <h1>Effective Web Design</h1>
    <p class="lead">We design and build fast, functional websites designed to turn visitors into customers.</p>
    <a href="<?php echo get_home_url();?>/contact/" class="btn btn-primary btn-lg">Send a message</a>
  </div>
</section>
<div class="container">
  <section class="ux">
    <div class="row">
      <div class="col-sm-8">
        <h2>User Experience Design</h2>
        <p>User experience design is like ergonomics. We all know there is a big difference between a badly designed scissors that hurts your fingers and a scissors that feels like an extension of your hand.</p>
        <p>Ergonomics is central to engineering and industrial design, just as user experience design is central to software engineering and web design.</p>
        <p>There is a saying in the ergonomics world:</p>
        <blockquote class="blockquote">
          <p>“When in doubt, throw it out.”</p>
        </blockquote>
        <p>This is how most users approach apps and websites, except they throw them out much more quickly than they’d throw away something more physical. As a website owner, the greatest risk is that your users will get frustrated or bored and close the browser tab, never to return again.</p>
        <p>Focusing on user experience (UX) helps to maximise conversions and boost customer engagement.</p>
        <p>Using market analysis, user testing, persona creation and user flows, we work to make your website as user friendly as possible. Wave goodbye to your high bounce rate.</p>
      </div>
    </div>
  </section>
  <section class="responsive">
    <div class="row">
      <div class="col-md-7">
        <h3>Responsive Web Design</h3>
        <p>Your laptop screen is a different size and shape from your phone’s screen, but your website needs to look great on
          both devices. This is where responsive web design comes in. </p>
          <p><span class="bold">More than 50%</span> of web traffic is now coming from mobile devices, and this growth trend is set to continue. </p>
        <p>Our websites are built with mobile in mind, and resize themselves to fit any screen size, from tiny mobiles to massive
          desktop monitors.</p>
          <!-- <a href="<?php echo get_home_url();?>/responsive-web-design/" class="btn btn-primary">More about responsive web design</a> -->
      </div>
      <div class="col-md-5">
        <img src="https://focalise.ie/wp-content/uploads/2017/05/responsive-layouts-2.jpg" alt="Responsive Web Design" class="img-fluid rounded">
      </div>
    </div>
  </section>
  <section class="ecommerce">
    <div class="row">
      <div class="col-md-7">
        <h3>E-Commerce</h3>
        <p>Create a web store that is open to accept orders and payments 24/7. Whether you've got 1 product or 10,000+ products
          to sell, we have a solution for you.</p>
      </div>
      <div class="col-md-5 hidden-sm-down">
        <img src="https://focalise.ie/wp-content/uploads/2017/05/e-commerce.jpg" alt="Ecommerce" class="img-fluid rounded">
      </div>
    </div>
  </section>
  <section class="secure-payments">
    <div class="row">
      <div class="col-md-7">
        <h3>Secure Payment Systems</h3>
        <p>Accept credit card payments
          <span class="bold">securely</span> without putting your customers at risk.</p>
          <p>We integrate secure payment gateways from <a href="https://stripe.com">Stripe</a>, <a href="https://paypal.com"> Paypal</a>, <a href="https://www.braintreepayments.com/">Braintree</a> and <a href="https://www.realexpayments.com/ie/">Realex</a> so you don't have to worry about storing and protecting your customers credit card details.</p>
      </div>
      <div class="col-md-5 hidden-sm-down">
        <img src="https://focalise.ie/wp-content/uploads/2017/05/credit-card-2.jpg" alt="Credit cards" class="img-fluid rounded">
      </div>
  </section>

  <section class="cms">
    <div class="row">
      <div class="col-md-8">
        <h3>Easy Content Management</h3>
        <p>You need a great way to organise the content on your website so you don’t need to contact a web developer every time
          you have a new promotion.
          <p>
            <a href="http://www.wordpress.org">WordPress</a> is the first choice for many of our clients. It&#8217;s easy to use, extensible and powers many
            of the most popular websites on the internet.
          </p>
          <p>We offer custom theme design services,
            <a href="https://focalise.ie/web-hosting/">web hosting</a> and also
            <a href="<?php echo get_home_url();?>/wordpress-training">WordPress training</a>.
      </div>
    </div>
  </section>
  <section class="landing-pages">

    <h3>Effective Landing Pages</h3>
    <p>Landing pages are web pages designed with a single objective in mind, carefully crafted to guide the user to take the
      next step, whether that be signing up to your service or entering a contest.</p>
    <p>Great landing page designs tend to minimise distractions and make process of signing as enjoyable and simple as possible.</p>
    <p>If your product needs a landing page,
      <a href="<?php echo get_home_url();?>/contact/">get in touch</a> for a chat today.</p>
      <a href="<?php echo get_home_url();?>/landing-page-design" class="btn btn-primary">More on Landing Pages</a>
  </section>
  <section class="our-process">
  <div class="col-md-9">
    <h3>The Process</h3>
    <p class="lead">We start with a strong vision of the finished product and that drives the whole project.</p>
    <p>There is more to a good website design than whether it looks good or not. Usability, structure and performance are all
      important factors to get a website to the top of the search results.</p>
    <p>We've established a tried and tested web design process to ensure that every website we build delivers great value for
      our clients:</p>
      <ol>
        <li>We start off with research into your target market and clarification of the key goals of the project.</li>
        <li>Once we have a solid plan, we develop our first draft of the website, and this acts as a basis for discussion and feedback.</li>
        <li>When the design is signed off, the draft design is developed into the final, fully featured website, ready for action.</li>
      </ol>
  </div>
  </section>
  <section class="call-out mt-5 mb-5">
    <h2 class="mb-3">Ready to get started?</h2>
    <a class="mb-3 btn btn-block btn-lg btn-primary mb-3" href="https://focalise.ie/contact/">Book your free consultation</a>
  </section>
  <section class="hosting">
    <div class="jumbotron">
      <div class="container">
        <h3>
          <i class="fa fa-bolt"></i> Managed Hosting Packages</h3>
        <p class="lead">Ensure your website responds quickly to your customers with our managed hosting packages.</p>
        <a href="<?php echo get_home_url();?>/web-hosting/" class="btn btn-outline-primary btn-lg">Web Hosting Info</a>
      </div>
    </div>
  </section>
  <div class="row">
    <div class="col-sm-6">
      <h3>Web Development Services</h3>
      <p class="lead">Take your business to the next level.</p>
      <p>We provide a wide range of web development services to support your business. </p>
      <a class="mb-5  btn btn-lg btn-block btn-outline-primary" href="https://focalise.ie/contact/">
        <i class="fa fa-cog fa-spin"></i> Start your project</a>
    </div>
    <div class="col-sm-6">
      <ul class="services-list list-group mt-4 mb-4">
        <li class="list-group-item">
          <a href="https://focalise.ie/how-to-improve-a-website">Website Improvements</a>
        </li>
        <li class="list-group-item">
          Marketing Automation
        </li>
        <li class="list-group-item">
          Social Media Integrations
        </li>
        <li class="list-group-item">
          Twitter Bots
        </li>
        <li class="list-group-item">
          Web Scrapers
        </li>
        <li class="list-group-item">
          HTTPS/SSL Certificate Installations
        </li>
      </ul>

    </div>
  </div>