<img src="<?php bloginfo('template_directory');?>/dist/images/wordpress-course.jpg" class="img-fluid" alt="WordPress Training">
<div class="jumbotron">
  <div class="container">
    <div class="col-md-10 offset-md-1">

      <h1>WordPress Training Dublin</h1>
      <p class="lead">Learn how to build a blog, a portfolio website, a product landing page, or any other kind of website. </p>
      <a class="btn btn-outline-primary btn-lg" href="<?php echo get_home_url();?>/contact/">
        Book a session
      </a>
    </div>
  </div>
</div>
<div class="container">
  <div class="col-md-10 offset-md-1">
    <section class="training-offer">
      <h2>Classes, 1-to-1 &amp; Online Training</h2>
      <p class="lead">If you are located in Dublin or Wicklow, we can schedule a 1-to-1 session at a time that suits you. We also offer small
        group lessons with up to 6 people per class for business.</p>
      <p class="lead">If you are located further away, but you'd still like to book a lesson, we can use online screen sharing and video
        chat to hold the lesson.</p>
    </section>


    <section class="what-is-wordpress">
      <h3>What is WordPress?</h3>
      <p>
        <a href="https://wordpress.org">WordPress</a> is an open source content management system. It makes it easy for you to manage a website without having
        to deal with code. WordPress is free to use, modify and commericialise.</p>
      <p>Originally WordPress was designed as blogging platform, but thanks to a huge community of users and developers, it
        has grown into a fullly featured web publishing platform. It can run nearly any kind of website, from a shop to a
        magazine, porfolio or blog.</p>
      <p>WordPress is easy to set up, manage and update. It is easy to add functionality to the site as your business develops,
        without having to hire a web developer.</p>
      <p>WordPress has become an immensely important tool for businesses across the globe, as it makes it much easier for non-technology
        companies to publish on the internet.</p>
      <p>Once you learn WordPress, you&#8217;ll be able to create website after website, easily. With WordPress, you can build
        sites for side projects, events, consulting services etc.</p>
      <p>Many businesses have room for improvement in their digital strategy, and WordPress is a marketing weapon of choice
        for some of the biggest brands in the world.
        <a href="https://www.sonymusic.com">Sony Music</a>,
        <a href="https://beyonce.com">Beyonce</a> and
        <a href="https://blogs.reuters.com/us/">Reuters</a> all run WordPress. </p>
    </section>
    <section class="wordpress-puts-you-in-control">
      <h4>Your own
        <em>www.brand.com</em>
      </h4>
      Running a website on your own domain name gives your brand a home on the internet, one that you control, rather than a page
      inside another site, like Facebook.</p>
    </section>
    <section class="get-started">
      <div class="jumbotron">
        <h3>Get started today</h3>
        <p>In-person lessons available in Dublin &amp; Wicklow Ireland, and online traning sessions are available around the
          world at a time that suits you.</p>
        <a class="btn btn-lg btn-outline-primary btn-block" href="<?php echo get_home_url();?>/contact/">
          Send a message to book a session
        </a>
      </div>
      </p>
    </section>
    <section class="topic-examples">
      <h1>Example Topics</h1>
    </section>
    <section class="domain-name">
      <h2>Buying your domain name</h2>
      <p>There are loads of domain registrars out there. We&#8217;ll show you our favourite registrars, how to get the cheapest
        price, and where you can pick up a .ie domain.</p>
      <p>We&#8217;ll walk through adding the domain to your website, creating sub-domains and adding multiple domains to the
        same site. </p>
    </section>
    <section class="web-host">
      <img src="<?php bloginfo('template_directory');?>/dist/images/choosing-a-web-host.jpg" class="img-fluid mb-4" alt="Choosing a web host">
      <h2>Choosing a web host</h2>
      <p>We&#8217;ll show you a couple of hosting options at different price points, with the more expensive packages offering
        improved performance, and more control.</p>
    </section>
    <section class="content-marketing">
      <h2>Content &amp; Marketing</h2>
      <p class="lead">Once you&#8217;ve built the frame of your website, what would you like to put on it?</p>
      <p>We&#8217;ll walk you through figuring out what the most effective content would be for your website, how to put it
        together and how to get it looking great.</p>
      <p>Content marketing is a mixture of science and art. Writing great content takes some work, but it gives your users a
        reason to stay on your site. It will encourage them to get involved with what you&#8217;re doing, and your site will
        rank nice and high in Google.</p>
      <p>We&#8217;ll cover content marketing strategies, types of content, and we&#8217;ll show you examples of great content
        and the success it generated.</p>
    </section>
    <section>
      <h2>Organising your Posts</h2>
      <p class="lead">There are plenty of ways to organise your posts.</p>
      <p>From categories to tags, you'll get a tour of how WordPress organises your content and how to make an easy to use website.</p>
    </section>
    <!-- <section>
      <h3>When to use pages or posts</h3>
      <p>Pages and posts, although very similar, behave slightly differently. We&#8217;ll explain when you might want to use
        posts and when it&#8217;s a page you&#8217;re looking for. </p>
    </section> -->
    <section>
      <h3>How to Add Images, Audio &amp; Video</h3>
      <p>You can put videos from YouTube, Vimeo or Vine straight into your posts, add images from Flickr or add an audio recording
        from your computer.</p>
      <p>WordPress keeps all the files that you upload easily accessible in the Media Library, where you can edit the files
        and add extra information.</p>
      <p>WordPress and a laptop makes for an amazing broadcasting combination.</p>
    </section>
    <section>
      <h3>Moderating Comments</h3>
      <p>If you like, you can let people leave comments on your posts, so they can ask questions and tell you what they&#8217;re
        thinking. You can choose to approve or disapprove each comment as they come in, so you&#8217;re always in control
        of what&#8217;s written on your website. </p>
    </section>
    <section>
      <h3>Dealing with Spam</h3>
      <p>There are some bad guys out there who have written computer programs that search the internet for WordPress sites,
        and once the program finds a website, it starts posting all sorts of nonsense comments, including links to nasty
        websites. We&#8217;ll teach you how to recognise this spam and show you how to deal with it. Once you&#8217;ve got
        an anti-spam system set up you won&#8217;t have to deal with it ever again.</p>
    </section>

  </div>
</div>
<div class="jumbotron jumbotron-fluid stripe-top mb-0">
  <div class="container pb-2">
    <h3>
      <i class="fa fa-check"></i> Get started today</h3>
    <p class="lead">Our WordPress lessons are available around the world, at a time that suits you.</p>
    <a href="<?php echo get_home_url();?>/contact/" class="btn btn-lg btn-outline-primary btn-block">
      <i class="fa fa-envelope-o"></i> Send a message to book a session</a>
  </div>
</div>
<div class="container">
  <div class="col-md-10 offset-md-1">
    <h2 style="margin-top: 2em;">Getting more advanced</h2>
    <img class="img-fluid mb-2 mt-3" src="<?php bloginfo('template_directory');?>/dist/images/metrics.jpg">
    <section>
      <h2>Learning Metrics</h2>
      <p>It&#8217;s pretty helpful to learn about what content on your site is the most popular with your visitors, and who
        your visitors are. Metrics can be helpful guides to building a great website. We&#8217;ll teach you how to add Google
        Analytics to your site in order to unlock a wealth of information, like:</p>
      <ul>
        <li>Who your visitors are.</li>
        <li>Where they come from.</li>
        <li>How long they stay on the site.</li>
        <li>Which of your posts are the most popular.</li>
        <li>What search terms people are using to find your site.</li>
      </ul>
    </section>
    <section>
      <h2>Installing and Customising Themes</h2>
      <p>WordPress is like Mystique from X-Men. It can look whatever way it wants. You can change your entire site layout and
        color scheme by installing one of the the many many free and commercial themes avaiable. Many of these themes have
        been put together by excellent designers and can look quite professional. Looking
        <em>truly</em> professional might take a bit more work in order to properly fit the design to your brand, but installing
        a theme is all thats needed for plenty of simple websites. </p>
      <p>We&#8217;ll help you find which wordpress theme you should use.</p>
    </section>
    <section>
      <h2>
        <i class="glyphicon glyphicon-user"></i> Users</h2>
      <p>Maybe you&#8217;d like to get a larger team working on your website. Once again WordPress makes it easy. You can create
        as many user accounts as you need for contributing authors or editors. We&#8217;ll show you how to create these accounts,
        and how to make sure your users can&#8217;t do anything to the website they&#8217;re not supposed to. </p>
    </section>
    <section>
      <h2>WordPress Plugins</h2>
      <p>So there&#8217;s 40,000+ plugins. Which ones do you need? We&#8217;ll look at some of the most popular plugins, from
        performance enhancing cache systems to social media integrations. There are also loads of
        <abbr title="Search Engine Optimisation">SEO</abbr> plugins. While they&#8217;re an easy way to add functionality to your site, plugins can cause a bit of
        trouble if you&#8217;re not careful. They can slow down WordPress and introduce some insecurities. We&#8217;ll show
        you what our favourite plugin setup looks like and explain how we got there.
      </p>
    </section>
    <section>
      <h2>WordPress
        <abbr title="Search Engine Optimisation">SEO</abbr> Tips</h2>
      <p>Google uses over 300 metrics in order to rank each website, so climbing the search results ladder can take a lot of
        fiddling, but the increased traffic that comes from the number 1 spot generally makes it worth all the hard work.
      </p>
    </section>
    <section>
      <h2>
        <i class="glyphicon glyphicon-link"></i> Backlinks and
        <abbr title="Search Engine Optimisation">SEO</abbr>
      </h2>
      <p>A backlink is a link on somebody elses website, pointing at your website. These links are important because they are
        what Google used to use in order to rank websites. The idea was, the more websites linked to
        <em>Website A</em> in their content, the more important
        <em>Website A</em> must be. </p>
      <p>Nowadays things are a lot more complicated. &#8220;Black Hat&#8221;
        <abbr title="Search Engine Optimisation">SEO</abbr> guys were gaming the old system, generating thousands of fake backlinks in order to trick Google. Google
        had to learn new ways to rank its search results, adding tons of new measurements like user behaviour, content analysis
        and page speed metrics in order to better sort the wheat from the chaff on a sprawling internet. </p>
      <p>We&#8217;ll dive into
        <abbr title="Search Engine Optimisation">SEO</abbr> in more detail if you&#8217;re interested, looking at a wide array of strategies to increase traffic.
      </p>
    </section>
    <section>
      <h2>
        <i class="glyphicon glyphicon-shopping-cart"></i> WordPress with a shopping cart</h2>
      <p>If you&#8217;d like to sell a product on your site, and have your website take payments, WordPress can save the day
        yet again. For those who are interested, we tour WooCommerce, an excellent free package that converts WordPress sites
        into fully functional web stores, complete with product listings, reviews, paypal integration and more. </p>
    </section>
    <section>
      <h2>
        <i class="glyphicon glyphicon-save"></i> Backups</h2>
      <p>If you&#8217;ve put a lot of time into a website, it makes sense to set up a backup system in case something happens
        to the site. We&#8217;ll show you some great
        <em>set and forget</em> backup tools that will keep your work safe.</p>
    </section>
    <section>
      <h2>
        <i class="glyphicon glyphicon-lock"></i> WordPress Security</h2>
      <p>Even better than restoring from a backup, you can ensure nothing happens to your site in the first place by keeping
        your site up to date at all times. </p>
      <p>Whenever a vunerability in WordPress is discovered by the WordPress development team, they will fix the problem, harden
        WordPress and send out an update notification to all the WordPress sites around the world. If you hit that &#8220;Update
        Now&#8221; button nice and often, your site will hopefully always be one step ahead of any attackers. We&#8217;ll
        talk about making updates automatic, plugin updates and theme updates, and well as installing
        <a href="https://focalise.xyz/top-wordpress-security-plugins/">security plugins</a>.
      </p>
    </section>
    <section>
      <h2>Start your WordPress journey</h2>
      <p>It&#8217;s a lot of fun to run a website, and WordPress makes it easier than ever. Get in touch today and we&#8217;ll
        have a chat about what you&#8217;d like to learn how to do.</p>
        
        <p>If you&#8217;re just starting your first website, we
        can start at the beginning, or if you&#8217;ve been running a website for a while and would like to take it to the
        next level, we can start with more advanced topics.</p>
        <a href="<?php echo get_home_url();?>/contact/" class="btn btn-lg btn-primary btn-block">Get in touch</a>
  </div>
  </section>
</div>
<!-- <div class="container">
  <div class="col-md-10 offset-md-1">
    <section class="in-person mb-4">
      <h2>In-Person Training</h2>
      <p class="lead">If you'd rather get your classes in person, there are 1 on 1 or group classes available at your business location,
        if you're situated in Dublin or Wicklow, Ireland.</p>
      
    </section>
  </div>
</div> -->
<!-- <section class="online-training book-your-session jumbotron">
  <div class="container">
    <h2 class="mb-2">Online WordPress Training</h2>
    <div class="row">
      <div class="col-md-12">
        <h4>Book a 60 minute training session</h4>
        <p class="lead">Online lessons are powered by Skype or Google Hangouts.</p>
        <a class="btn btn-primary btn-lg btn-block mb-4" href="<?php echo get_home_url();?>/contact/">Book your session</a>
        <h3>Price: €75</h3>
      </div>
    </div>
  </div>
</section> -->
<script type="application/ld+json">
  {
    "@context": "https://schema.org/",
    "@type": "Product",
    "name": "WordPress Training",
    "image": "https://focalise.ie/wp-content/themes/focalise/dist/images/wordpress-course.jpg.pagespeed.ce._H0F4IrlMX.jpg",
    "description": "Need some help with Wordpress? Chat to an expert today and learn at a pace that suits you. Available worldwide. ",
    "brand": {
      "@type": "Thing",
      "name": "Focalise"
    },
    "offers": {
      "@type": "Offer",
      "priceCurrency": "EUR",
      "price": "75.00",
      "priceValidUntil": "2016-12-31",
      "availability": "https://schema.org/InStock",
      "seller": {
        "@type": "Organization",
        "name": "Focalise"
      }
    }
  }
</script>
<script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "Course",
    "name": "Introduction to WordPress",
    "description": "Introductory WordPress course suitable for all.",
    "provider": {
      "@type": "Organization",
      "name": "Focalise",
      "sameAs": "https://focalise.ie"
    }
  }
</script>
<script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "Course",
    "name": "Advanced WordPress",
    "description": "Stepping it up a level. Learn how to get even more out of the best CMS around.",
    "provider": {
      "@type": "Organization",
      "name": "Focalise",
      "sameAs": "https://focalise.ie"
    }
  }
</script>