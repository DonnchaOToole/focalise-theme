<div class="jumbotron mt-5">
  <div class="container">
    <h1>Featured Web Design Projects</h1>
    <p class="lead">Every project is different and we strive to build <span class="bold">effective</span> digital experiences that <span class="bold">boost business</span> for each of our clients. </p>
  </div>
</div>

<div class="container">

<section class="farmgas case-study">
<div class="row">
  <div class="col-6">
    <h4>FarmGas</h4>
    <p class="lead">We designed and developed a responsive website to help FarmGas find new business partners. </p>
    <!-- <a target="_blank" href="https://farmgas.ie" class="btn btn-primary">Visit website in a new tab</a> -->
  </div>
  <div class="col-6">
    <img src="<?php bloginfo('template_directory');?>/dist/images/farmgas.png" alt="Image of FarmGas Website" class="img-fluid case-study-thumb">
  </div>
</div>
  
</section>
<section class="lucynuzumweddings case-study">
<div class="row">
  <div class="col-6">
  <h4 class="card-title">Lucy Nuzum Weddings</h4>
  <p class="lead">Lucy needed a mobile ready website to display her photography style to newly engaged couples. </p>
  <a target="_blank" href="http://lucynuzumweddings.com" class="btn btn-primary">Visit website in a new tab</a>
  </div>
  <div class="col-6">
    <img src="<?php bloginfo('template_directory');?>/dist/images/lucynuzumweddings.png" alt="Image of Lucy Nuzum Weddings Website" class="img-fluid case-study-thumb">
  </div>
</div>
</section>
</div>