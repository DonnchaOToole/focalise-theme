            <?php 
if ( have_posts() ) : ?>
               
                    <div class="mt-5 jumbotron ">
                    <div class="container">
                        <h1 class="archive-title">Tutorials</h1>
                        <p class="lead">Free Video Tutorials</p>
                    </div>
                    <?php
// Display optional category description
 if ( category_description() ) : ?>
                        <div class="archive-meta">
                            <?php echo category_description(); ?>
                        </div>
                    </div>
                    </div>
                        <?php endif; ?>
           
           <div class="container">
                <?php

// The Loop
while ( have_posts() ) : the_post(); ?>
                    <div class="col-sm-8">
                        <div class="card mb-4">
                            <?php echo get_the_post_thumbnail( $page->ID, 'full', array('class' => 'card-img-top img-fluid') ); ?>
                                <div class="card-block">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute();?>" class="card-title">
                                        <h2 class="card-title"><?php the_title(); ?></h2>
                                    </a>
                                    <p class="card-text">
                                        <?php the_excerpt(); ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-primary card-link btn-outline-primary">Watch Tutorial</a>
                                </div>
                        </div>
                    </div>
                    <!--                     <div class="entry">
                        <?php the_content(); ?>
                    </div> -->
                    <?php endwhile; 

else: ?>
                        <p>Sorry, no posts matched your criteria.</p>
                        <?php endif; ?>
        </div>
    </section>

           </div>