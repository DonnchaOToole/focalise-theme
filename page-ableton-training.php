<img src="<?php bloginfo('template_directory');?>/dist/images/ableton-academy.jpg" alt="" class="img-fluid">
<div class="container">
    <section class="ableton-intro">
        <div class="row">
            <div class="col-sm-7">
                <h1>The ultimate music tool</h1>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum tempora laboriosam eos, reprehenderit ad, quasi rerum voluptatibus quaerat eligendi dolorum, aliquam repudiandae!</p>
            </div>
            <div class="col-sm-5">
                <img src="http://placehold.it/500x500" alt="" class="img-fluid">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h2>Killer beats</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum tempora laboriosam eos, reprehenderit ad, quasi rerum voluptatibus quaerat eligendi dolorum, aliquam repudiandae!</p>
            </div>
            <div class="col-sm-6">
                <h2>Lusious soundscapes</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum tempora laboriosam eos, reprehenderit ad, quasi rerum voluptatibus quaerat eligendi dolorum, aliquam repudiandae!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h2>Hands on with MIDI</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum tempora laboriosam eos, reprehenderit ad, quasi rerum voluptatibus quaerat eligendi dolorum, aliquam repudiandae!</p>
            </div>
            <div class="col-sm-6">
                <h2>Playing Live</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum tempora laboriosam eos, reprehenderit ad, quasi rerum voluptatibus quaerat eligendi dolorum, aliquam repudiandae!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h2>Mixing</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum tempora laboriosam eos, reprehenderit ad, quasi rerum voluptatibus quaerat eligendi dolorum, aliquam repudiandae!</p>
            </div>
            <div class="col-sm-6">
                <h2>Compression</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum tempora laboriosam eos, reprehenderit ad, quasi rerum voluptatibus quaerat eligendi dolorum, aliquam repudiandae!</p>
            </div>
        </div>
    </section>
</div>
