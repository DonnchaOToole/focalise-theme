# User Experience Design

User experience design is like ergonomics. We all know there is a big difference between a badly designed scissors that hurts your fingers and a scissors that feels like an extension of your hand.

Ergonomics is central to engineering and industrial design, just as user experience design is central to software engineering and web design.

There is a saying in the ergonomics world:

>“When in doubt, throw it out.”

This is how most users approach apps and websites, except they throw them out much more quickly than they’d throw away something more physical. As a website owner, the greatest risk is that your users will get frustrated or bored and close the browser tab, never to return again.

Focusing on user experience (UX) helps to maximise conversions and boost customer engagement.

Using market analysis, user testing, persona creation and user flows, we work to make your website as user friendly as possible. Wave goodbye to your high bounce rate.

