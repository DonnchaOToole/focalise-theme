<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- WORLD NEWS -->
            <div class="jumbotron news-jumbotron
">
                <div class="container">
                    <h2>World News</h2>
                </div>
            </div>
            <?php $args = array(
	'posts_per_page'   => 5,
	'category'         => '20',
);
$worldnews_array = get_posts( $args ); 

foreach ($worldnews_array as $post) : setup_postdata($post);?>
                <div class="news-panel row">
                    <div class="col-sm-3">
                        <?php the_post_thumbnail('thumbnail', array('class' => 'img-fluid')); ?>
                    </div>
                    <div class="col-sm-9">
                        <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                        <p>
                            <?php the_excerpt();?>
                        </p>
                        <a href="<?php the_permalink();?>">
                            <button class="btn pull-right btn-default">Read more</button>
                        </a>
                    </div>
                </div>
                <!-- end the loop -->
                <?php endforeach;
		wp_reset_postdata();?>
                    <!-- Ireland and UK News -->
                    <div class="jumbotron news-jumbotron">
                        <div class="container">
                            <h2>Ireland & UK News</h2>
                        </div>
                    </div>
                    <?php $args = array(
	'posts_per_page'   => 5,
	'category'         => '15',
);
$worldnews_array = get_posts( $args ); 

foreach ($worldnews_array as $post) : setup_postdata($post);?>
                        <div class="news-panel row">
                            <div class="col-sm-3">
                                <?php the_post_thumbnail('thumbnail', array('class' => 'img-fluid img-rounded')); ?>
                            </div>
                            <div class="col-sm-9">
                                <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                <p>
                                    <?php the_excerpt();?>
                                </p>
                                <a href="<?php the_permalink();?>">
                                    <button class="btn pull-right btn-default"><i class="glyphicon chevron-left"></i>Read more</button>
                                </a>
                            </div>
                        </div>
                        <!-- end the loop -->
                        <?php endforeach;
		wp_reset_postdata();?>
        </div>
    </div>
</div>
<!-- /main container -->
