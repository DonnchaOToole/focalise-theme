<?php
/**
* Links Category Template
*/

?>
    <section id="primary" class="site-content container">
        <div id="content" role="main">
            <?php 
// Check if there are any posts to display
if ( have_posts() ) : ?>
                <header class="archive-header">
                    <h1 class="archive-title">Tools</h1>
                    <p class="lead">Useful tools to help your business.</p>
                    <?php
// Display optional category description
 if ( category_description() ) : ?>
                        <div class="archive-meta">
                            <?php echo category_description(); ?>
                        </div>
                        <?php endif; ?>
                </header>
                <?php

// The Loop
while ( have_posts() ) : the_post(); ?>
                    <div class="col-sm-12">
                        <div class="card">
                            <?php echo get_the_post_thumbnail( $page->ID, 'full', array('class' => 'card-img-top img-fluid') ); ?>
                                <div class="card-block">
                                    <a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute();?>" class="card-title">
                                        <h2><?php the_title(); ?></h2>
                                    </a>
                                    <div class="card-text">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!--                     <div class="entry">
                        <?php the_content(); ?>
                    </div> -->
                    <?php endwhile; 

else: ?>
                        <p>Sorry, no posts matched your criteria.</p>
                        <?php endif; ?>
        </div>
    </section>
