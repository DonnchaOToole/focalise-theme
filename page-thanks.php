<div class="container">
    <div class="page-header mt-5">
        <h1 class="pt-4">Thanks, your message has been sent.</h1>
        <p class="lead">We will get in touch with you as soon as possible.</p>
        <p>If you like, you can also follow us on <a href="https://www.facebook.com/focalise.ie/">Facebook</a> and <a href="https://twitter.com/focalise_ie">Twitter</a></p>
    </div>
</div>
