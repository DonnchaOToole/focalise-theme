<div class="container mt-5">
<div class="col-md-10 offset-md-1">
    <?php while (have_posts()) : the_post(); ?>
    <h1 class="pt-5 entry-title">
        <?php the_title(); ?>
    </h1>
    <article <?php post_class(); ?>>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
    </article>
    <?php comments_template('/templates/comments.php'); ?>
    <?php endwhile; ?>
</div>
</div>
