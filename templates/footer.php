<footer class="mt-5">
  <section class="newsletter pt-5">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <h4>Get Free Digital Marketing Tutorials</h4>
          <p class="lead">Enter your email to get free video tutorials delivered straight to your inbox.</p>
          <div id="mc_embed_signup">
            <form action="//fourcandles.us7.list-manage.com/subscribe/post?u=5ad740079e1860da55c5d2271&amp;id=0b9288e798" method="post"
              id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
              <div class="form-group" id="mc_embed_signup_scroll">
                <input type="email" value="" name="EMAIL" class="email form-control input-lg" id="mce-EMAIL" placeholder="Your email address"
                  required>
                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                  <input type="text" name="b_5ad740079e1860da55c5d2271_0b9288e798" tabindex="-1" value="">
                </div>
                <div class="clear newsletter-submit">
                  <input type="submit" value="Get Free Tutorials" name="subscribe" id="mc-embedded-subscribe" class="btn btn-lg btn-outline-primary">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="footer-top">
    <div class="container">
      <div class="row">
        <div id="contact" class="footer-details col-md-6">
          <h3 class="">Contact</h3>
          <div class="phone">
            <a href="tel:00353834486980">+353 (0) 83 448 6980</a>
          </div>
          <div class="email">
            <a href="mailto:hello@focalise.ie">hello@focalise.ie</a>
          </div>
          <a class="btn mt-2 mb-2" href="<?php echo get_home_url();?>/contact/">SEND A MESSAGE</a>
          <br>
          <h4 class="mt-4">Address</h4>
          <div itemprop="address" itemscope class="contact-details">
            St. Columbs
            <br> Sidmonton Road
            <br> Greystones
            <br> Co. Wicklow
            <br> Ireland
            </span>
          </div>
        </div>
        <div class="col-md-6">
          <h3>Links</h3>
          <ul class="footer-links">
            <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/landing-page-design/">Landing Page Design</a></li>
            <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/seo-services/">SEO Services</a></li>
            <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/wordpress-support/">WordPress Support</a></li>
            <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/training/">Training</a></li>
            <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/seo-tips/">SEO Tips</a></li>
            <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/trading-online-voucher-scheme/">Trading Online Voucher Scheme</a></li>
            <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/google-suite/">G Suite</a></li>  
            <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/resources/">Resources</a></li>
            <!-- <li><i class="fa fa-circle-o footer-icon"></i><a href="<?php echo get_home_url();?>/computer-repair/">Computer Repair</a></li> -->
            <!-- <li><i class="fa fa-angle-right footer-icon"></i><a href="<?php echo get_home_url();?>/web-design-wicklow/">Web Design Wicklow</a></li> -->
            <!-- <li><i class="fa fa-angle-right footer-icon"></i><a href="<?php echo get_home_url();?>/web-design-greystones/">Web Design Greystones</a></li> -->
            </ul>
        </div>
      </div>
  </section>
  <section id="footer-bottom">
    <div class="container">
      <div class="text-center">
        <div class="made-with-love">
          <span class="made-with">Made with</span>
          <span class="heart">
            <i class="fa fa-heart love-heart"></i>
          </span>
          <span class="in-wicklow">in Wicklow</span>
        </div>

      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="btn-group d-flex justify-content-center mb-3">
            <a class='btn btn-primary' href="https://www.facebook.com/focalise.ie/">
              <i class="fa fa-facebook"></i>
            </a>
            <a class='btn btn-primary' href="https://twitter.com/focalise_ie">
              <i class="fa fa-twitter"></i>
            </a>
            <!-- <a class='btn btn-primary' href="https://focalise.ie/instagram/">
              <i class="fa fa-instagram"></i>
            </a> -->
            <a class='btn btn-primary' href="https://www.linkedin.com/company/10474380/">
              <i class="fa fa-linkedin"></i>
            </a>
          </div>
          <div class="text-center">
            <div class="copyright">
              &copy; 2015-<?php echo date("Y"); ?>
              <a href="https://focalise.ie">focalise.ie</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</footer>
<script>
  (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
      m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

  ga('create', 'UA-56357572-2', 'auto');
  ga('send', 'pageview');
</script>