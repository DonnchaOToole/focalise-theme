<nav class="navbar fixed-top navbar-toggleable-md navbar navbar-inverse bg-primary">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?php echo get_home_url();?>">Focalise</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo get_home_url();?>/about/">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo get_home_url();?>/web-design/">Web Design</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo get_home_url();?>/web-hosting/">Web Hosting</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link" href="<?php echo get_home_url();?>/work/">Featured Work</a>
            </li>  -->
            <!--<li class="nav-item">
                <a class="nav-link" href="<?php echo get_home_url();?>/training">Training</a>
            </li>-->
            <!-- <li class="nav-item">
                <a class="nav-link" href="<?php echo get_home_url();?>/tutorials/">Tutorials</a>
            </li> -->
            <!-- <li class="nav-item">
                <a class="nav-link" href="<?php echo get_home_url();?>/resources/">Resources</a>
            </li>  -->
        </ul>
        <!-- <div class="float-xs-right">
                <a class="nav-contact-btn btn align-middle btn-no-padding text-white" href="mailto:hello@focalise.ie"><i class="fa fa-envelope-o mr-2"></i>hello@focalise.ie</a>
        </div> -->
    </div>
</nav>
