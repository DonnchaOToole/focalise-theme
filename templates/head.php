<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MF39XT2');</script>
<!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="p:domain_verify" content="a29955b3deed627776a627ceb09184ef" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google-site-verification" content="vvD644gcT4PR5xZxX_Xi7vfNykS5HV4NVCdhNwvh3tI" />
    <meta name="google-site-verification" content="vvD644gcT4PR5xZxX_Xi7vfNykS5HV4NVCdhNwvh3tI" />
    <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2189318694638353');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=2189318694638353&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

    <script async rel="preload" src="https://use.fontawesome.com/d07be4e63e.js"></script>
    <script async rel="preload" src="https://use.typekit.net/nlk5oaz.js"></script>
    <script>
    try {
        Typekit.load({
            async: true
        });
    } catch (e) {}
    </script>
    <meta name="p:domain_verify" content="a29955b3deed627776a627ceb09184ef" />
    <?php wp_head(); ?>

<!-- <script>
    (function(d, w, c) {
        w.ChatraID = 'dyhTkPfrs827abqSy';
        var s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
        + '//call.chatra.io/chatra.js';
        if (d.head) d.head.appendChild(s);
    })(document, window, 'Chatra');
</script>
<script>
window.ChatraSetup = {
    colors: {
        buttonBg: '#24a8fa'    /* chat button background color */
    }
};
</script> -->
</head>
