<?php use Roots\Sage\Titles; ?>
<div class="container">
<div class="row">
<div class="col-md-8 offset-md-2">
<div class="page-header pt-5 mt-5">
  <h1 class="mt-4"><?= Titles\title(); ?></h1>
</div>
</div>
</div>
</div>
