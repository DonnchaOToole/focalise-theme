<div class="text-center testimonials-header">
  <h3>Testimonials</h3>
</div>
<div class="testimonial">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <img src="https://focalise.ie/wp-content/uploads/2017/11/the-happy-pear-logo.png" alt="The Happy Pear Logo" class="img-fluid happypear-tesimonial-logo">
        <img class="mt-4 img-fluid rounded" alt="Photo of David and Stephen Flynn, owners of The Happy Pear in Greystones, Co. Wicklow." src="<?php bloginfo('template_directory');?>/dist/images/the-happy-pear.jpg">
      </div>
      <div class="col-md-7 mt-2">
        <blockquote class="blockquote">
          We have been working with Donncha for the last 10 years and he is brilliant, very fast and insightful and wonderful to deal
          with! We highly recommend him!
          <br>
          <div class="mt-3 blockquote-footer">
            <cite>Stephen Flynn,
              <a href="https://thehappypear.ie">The Happy Pear</a>
            </cite>
          </div>
        </blockquote>
      </div>
    </div>
  </div>
</div>
<div class="testimonial">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <img src="<?php bloginfo('template_directory');?>/dist/images/farmgas-logo.jpg" alt="FarmGas Logo" class="img-fluid farmgas-tesimonial-logo">
        <img src="<?php bloginfo('template_directory');?>/dist/images/stephen.jpg" alt="Photo of Stephen Branagan" class="img-fluid rounded">
      </div>
      <div class="col-md-7 mt-2">
        <blockquote class="blockquote">
          We have worked with Dunny on everything from website design to graphics, to IT infrastructure and have found him to be a
          very valuable addition to our team. Very knowledgeable, very responsive and nothing is too much trouble. Highly
          recommended!
          <div class="mt-3 blockquote-footer">
            <cite>Stephen Branagan,
              <a href="https://farmgas.ie">FarmGas</a>
            </cite>
          </div>
        </blockquote>
      </div>
    </div>
  </div>
</div>
