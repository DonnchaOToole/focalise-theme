<div class="jumbotron jumbotron-fluid bg-inverse text-white">
    <div class="container">
        <h1 class="text-white mt-5">Training</h1>
        <p class="lead">Gain an edge with Focalise Training.</p>
    </div>
</div>
<section class="courses">
    <div class="container">
        <div class="row my-2">
            <div class="col-sm-6">
                <div class="card">
                    <img class="card-img-top img-fluid" src="<?php bloginfo('template_directory');?>/dist/images/wordpress-course.jpg" alt="WordPress Training">
                    <div class="card-block">
                        <h4 class="card-title title">Wordpress Training</h4>
                        <p class="card-text">Learn the most popular publishing platform in the world.</p>
                        <a href="<?php echo get_home_url();?>/wordpress-training" class="btn btn-outline-primary">WordPress Training</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <img class="card-img-top img-fluid" src="https://focalise.ie/wp-content/uploads/2016/01/grow-your-audience-2.jpg" alt="Search Engine Optimisation (SEO)">
                    <div class="card-block">
                        <h4 class="card-title title">Search Engine Optimisation</h4>
                        <p class="card-text">Learn how to improve your rankings in the search results.</p>
                        <a href="<?php echo get_home_url();?>/seo-training" class="btn btn-outline-primary">SEO Training</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-2">
            <div class="col-sm-6">
                <div class="card">
                    <img class="card-img-top img-fluid" src="<?php bloginfo('template_directory');?>/dist/images/social-media-training.jpg" alt="Social Media Training">
                    <div class="card-block">
                        <h4 class="card-title title">Social Media</h4>
                        <p class="card-text">Crash Courses in Facebook, Instagram, Snapchat and more.</p>
                        <p class="card-text">Learn how to build a following and create some hype using just a smartphone or laptop.</p>
                        <a href="<?php echo get_home_url();?>/social-media-training" class="btn btn-outline-primary">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <img class="card-img-top img-fluid" src="<?php bloginfo('template_directory');?>/dist/images/learn-facebook-adverts.png" alt="Learn Facebook Adverts">
                    <div class="card-block">
                        <h4 class="card-title title">Facebook Adverts</h4>
                        <p class="card-text">Learn how to drive traffic to your business page with Facebook Adverts.</p><p class="cart-text">Learn how to position your ads so they are seen by the people who would be most interested.</p>
                        <a href="<?php echo get_home_url();?>/contact/" class="btn btn-outline-primary">Book a session</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
