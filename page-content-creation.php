<!--<img rel="preload" src="<?php bloginfo('template_directory');?>/dist/images/video-production2.jpg" alt="" class="img-fluid">-->
<section class="video-hero">
<div class="mt-5 d-flex justify-content-center">    
    <h1>Content Creation</h1>
</div>
</section>

    <div class="embed-responsive embed-responsive-16by9 m-b-2">
        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/102935516?title=0&byline=0&portrait=0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div>
</div>
<div class="container mt-5">
    <div class="row">
        <div class="col-sm-6">
            <img class="img-fluid" src="<?php bloginfo('template_directory');?>/dist/images/motion-design/.gif">
        </div>
        <div class="col-sm-6">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://drive.google.com/file/d/0B4iSbJFJuW1zWUM0c1Zrc3lwODQ/preview"></iframe>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h3 class="pt-4"><a href="<?php echo get_home_url();?>/motion-design/">Motion Design</a></h3>
            <p>Movement catches the eye. Whatever your idea, we can tell the story in motion.</p>
        </div>
        <div class="col-md-6">
            <h3 class="pt-4">Explainer Videos</h3>
            <p>If you product is complex, your customers may have trouble understanding what it is all about. Explainer videos are a great way to quickly introduce your product to new visitors.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <img src="https://focalise.ie/wp-content/uploads/2017/05/digital-marketing.jpg" alt="Digital Marketing" class="img-fluid">
            <h3 class="pt-4">Digital Marketing</h3>
            <p>We know where to place your video to get as many viewers as possible. We have lots of experience with YouTube, Vimeo, Facebook, Google Plus, Twitter and self hosted content.</p>
        </div>
        <!--<div class="col-md-6">
            <h3 class="pt-4">Video Editing</h3>
            <p>We provide an affordable and professional video editing service. Simply upload the files to us and let us handle the editing and post production. Get in touch for a chat and we can figure out how we can help you with your project. </p>
        </div>-->
         <div class="col-md-6">
            <h3 class="pt-4"><a href="<?php echo get_home_url();?>podcast-editing-service">Podcast Editing</h3></a>
            <p>A straightforward podcast editing service. Simply send your recordings and recieve your show ready to upload.</p>
        </div>
    </div>
    <a href="contact" class="btn btn-outline-primary btn-block btn-lg cta m-b-2">
        
    Start your project today</a>
</div>
