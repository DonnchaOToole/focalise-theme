<div class="jumbotron jumbotron-fluid mb-4 gradient-overlay">
  <div class="container">
    <h3 class="text-white mt-5">How to improve a website</h3>
    <p class="lead">5 tips to help your website succeed.</p>
  </div>
</div>
<div class="container article">
<h5>1. Don't make your users <em>think</em>.</h5>
<p>Web interfaces should be straight-forward and easy to use for every user, not just the computer geeks. Visual cues and a
  clear structure are important to help your users find what they are looking for.</p>
<p><span class="bold">User-centric design</span> has become the standard approach for modern web projects. If a user doesn't notice a feature,
  it might as well not exist.</p>
<h5>2. Create Compelling Content</h5>
<p>Content is often more important than the design that contains it. Your content should be credible and easy to read. Web users
  like to scan through content rather than read every line and the most successful content takes that into consideration.
</p>
<h5>3. Load Quickly</h5>
<p>Web users don't have a lot of patience.</p>
<p>A slow loading page can be very frustrating for the user; everyone has something better to do than sit around waiting for a sluggish webpage to load. Google
  have also stated that websites that load too slowly will not rank well in their search results, because they know that users don't like waiting. </p>

  <p>In fact, Google <a href="https://www.thinkwithgoogle.com/marketing-resources/data-measurement/mobile-page-speed-new-industry-benchmarks/">have estimated</a> that as page load time goes from one second to seven seconds, the probability of a mobile site visitor bouncing increases by <span class="bold">113%.</span></p>
  <img class="img-fluid" src="<?php bloginfo('template_directory');?>/dist/images/google-mobile-benchmarks.jpg" alt="Infographic of Google's mobile page load speed benchmarks.">
  <p>There are plenty of things you can do to speed up a slow loading website. The first trick is to ensure that the images on the page are as optimised as possible. It also helps a lot of run your website on a fast server and utilise a CDN (content distribution network). Have a look at our <a target="_blank" href="https://focalise/web-hosting">managed hosting packages</a> if you would like to give your website a boost.</p>
  <p>If you would like to check how quickly your web page is loading and compare it with other websites in your industry, head over to <a target="_blank"href="https://testmysite.withgoogle.com/">Test My Site</a> by Google and enter your website address for a straight-forward report. For a more detailed report, have a look at <a target="_blank" href="https://gtmetrix.com>">GTMetrix.</a></p>
  <h5>Dive a little deeper and get the edge on the competition.</h5>
  <p>Getting a little bit more technical, here are some more ways you can speed up a slow website:</p>
  <ul>
    <li>Minimise the amount of HTTP requests the website makes.</li>
    <li>Reduce the server response time.</li>
    <li>Enable compression. </li>
    <li>Minify CSS, JS and HTML.</li>
    <li>Remove unnecessary plugins.</li>
    <li>Upgrade to HTTP/2.</li>
    <li>Optimise your database.</li>
  </ul>
<h5>4. Test for bugs</h5>
<p>In the modern era, your web visitors are using all kinds of devices, from phones and laptops to games consoles and smart
  TVs. Your website should look great on every device, so your website design should be bug tested on these devices to ensure
  you are not frustrating users with broken pages.</p>
<h5>5. Impress Google</h5>
<p>As the clear leader in the search engine world, Google are a factor that every website owner needs to consider. Structuring
  and optimising your website and content to impress Google will help your site rank well, which in turn will drive visitors
  to your website. If you need some help improving your search rankings, have a look at our <a href="https://focalise.ie/seo-services/">SEO services.</a></p>
</div>